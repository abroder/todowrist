#include <pebble.h>

#include "./pebble-utils/strings/strings.h"

#define SCROLL_PAUSE 500
#define SCROLL_LIMIT 10

#define CELL_HEIGHT 44
#define IMAGE_MARGIN 5

#define DICTATION_BUFFER_SIZE 512

typedef enum {
  AppKeyTasksLength = 0,
  AppKeyTask = 1,
  AppKeyTaskId = 2,
  AppKeyTasksDone = 3
} AppKeys;

typedef struct task_t {
  uint32_t id;
  char *content;
  bool complete : 1;
  bool active: 1;    /* Currently highlighted & being animated */
} task_t;

typedef struct tasks_t {
  uint8_t size;
  uint8_t capacity;
  task_t **tasks;
} tasks_t;

static Window *s_window;
static MenuLayer *s_tasks_layer;
static StatusBarLayer *s_status_bar;

static tasks_t *s_tasks;

static uint8_t s_scroll_position;
static AppTimer *s_scroll_timer;

static bool s_interactive;

static GBitmap *s_box_checked;
static GBitmap *s_box_unchecked;

static GColor8 s_normal_palette[] = { { GColorBlackARGB8 }, { GColorClearARGB8 } };
static GColor8 s_gray_palette[] = { { GColorDarkGrayARGB8 }, { GColorClearARGB8 } };
static GColor8 s_inverted_palette[] = { { GColorWhiteARGB8 }, { GColorClearARGB8 } };

static void init();
static void deinit();

static void main_window_load(Window *);
static void main_window_unload(Window *);

static void menu_layer_draw_row(GContext *, const Layer *, MenuIndex *, void *);
static uint16_t menu_layer_get_num_rows(struct MenuLayer *, uint16_t, void *);
static void menu_layer_selection_changed(struct MenuLayer *, MenuIndex, MenuIndex, void *);
static void menu_layer_select_click(struct MenuLayer *, MenuIndex *, void *);
static void menu_layer_select_long_click(struct MenuLayer *, MenuIndex *, void *);
static int16_t menu_layer_get_cell_height(struct MenuLayer *, MenuIndex *, void *);

static void dictation_session_callback(DictationSession *, DictationSessionStatus, char *, void *);

static void start_scroll();
static void update_scroll(void *);

static void tasks_create(uint8_t);
static void tasks_append(uint32_t, char *);
static void tasks_destroy();

static void inbox_received_callback(DictionaryIterator *, void *);
static void inbox_dropped_callback(AppMessageResult, void *);
static void outbox_sent_callback(DictionaryIterator *, void *);
static void outbox_failed_callback(DictionaryIterator *, AppMessageResult, void *);

int main() {
  init();
  app_event_loop();
  deinit();
}

static void init() {
  s_window = window_create();
  window_set_window_handlers(s_window, (WindowHandlers) {
    .load = main_window_load,
    .unload = main_window_unload,
  });
  window_stack_push(s_window, true);

  app_message_register_inbox_received(inbox_received_callback);
  app_message_register_inbox_dropped(inbox_dropped_callback);
  app_message_register_outbox_failed(outbox_failed_callback);
  app_message_register_outbox_sent(outbox_sent_callback);

  /* Outbox: new task buffer & key */
  app_message_open(app_message_inbox_size_maximum(), DICTATION_BUFFER_SIZE + 4);
}

static void deinit() {
  window_destroy(s_window);
}

static void main_window_load(Window *window) {
  Layer *root_layer = window_get_root_layer(s_window);
  s_status_bar = status_bar_layer_create();
  status_bar_layer_set_separator_mode(s_status_bar, StatusBarLayerSeparatorModeDotted);
  layer_add_child(root_layer, status_bar_layer_get_layer(s_status_bar));

  GRect root_frame = layer_get_frame(root_layer);
  GRect status_bar_frame = layer_get_frame(status_bar_layer_get_layer(s_status_bar));
  GRect tasks_frame = GRect(0, status_bar_frame.size.h, root_frame.size.w, root_frame.size.h - status_bar_frame.size.h);

  s_tasks_layer = menu_layer_create(tasks_frame);
  menu_layer_set_click_config_onto_window(s_tasks_layer, s_window);
  menu_layer_set_callbacks(s_tasks_layer, NULL, (MenuLayerCallbacks) {
    .draw_row = menu_layer_draw_row,
    .get_num_rows = menu_layer_get_num_rows,
    .selection_changed = menu_layer_selection_changed,
    .select_click = menu_layer_select_click,
    .select_long_click = menu_layer_select_long_click,
    .get_cell_height = menu_layer_get_cell_height
  });
  layer_add_child(root_layer, menu_layer_get_layer(s_tasks_layer));

#ifdef PBL_COLOR
  status_bar_layer_set_colors(s_status_bar, GColorDarkCandyAppleRed, GColorWhite);
  menu_layer_set_highlight_colors(s_tasks_layer, GColorDarkCandyAppleRed, GColorWhite);
#endif

  // create placeholder task
  tasks_create(1);
  tasks_append(0, strdup("Sync tasks"));
  menu_layer_reload_data(s_tasks_layer);

  s_interactive = false;

  s_box_checked = gbitmap_create_with_resource(RESOURCE_ID_BOX_CHECKED);
  s_box_unchecked = gbitmap_create_with_resource(RESOURCE_ID_BOX_UNCHECKED);
}

static void main_window_unload(Window *window) {
  tasks_destroy();
  status_bar_layer_destroy(s_status_bar);
  menu_layer_destroy(s_tasks_layer);
}

static void menu_layer_draw_row(GContext *ctx, const Layer *cell_layer, MenuIndex *cell_index, void *callback_context) {
  if (!s_tasks || cell_index->row > s_tasks->size) return;

  task_t *task = s_tasks->tasks[cell_index->row];
  uint8_t offset = 0;
  bool highlighted = menu_cell_layer_is_highlighted(cell_layer);
  if (highlighted) {
    int16_t limit = strlen(task->content) - SCROLL_LIMIT - s_scroll_position;
    if (limit > 0) {
      offset += s_scroll_position;
    } else {
      task->active = false;
    }
  }

  GRect bounds = layer_get_bounds(cell_layer);
  GPoint cursor = { bounds.origin.x + IMAGE_MARGIN, bounds.origin.y };

  // set color
  GColor8 *palette;
  if (highlighted) {
    palette = s_inverted_palette;
    graphics_context_set_text_color(ctx, GColorWhite);
    graphics_context_set_stroke_color(ctx, GColorWhite);
  } else {
    palette = (task->complete) ? s_gray_palette : s_normal_palette;

    GColor content_color = (task->complete) ? GColorDarkGray : GColorBlack;
    graphics_context_set_text_color(ctx, content_color);
    graphics_context_set_stroke_color(ctx, content_color);
  }

  // draw checkbox
  GBitmap *checkbox = (task->complete) ? s_box_checked : s_box_unchecked;
  GRect checkbox_frame = gbitmap_get_bounds(checkbox);
  gbitmap_set_palette(checkbox, palette, false);
  graphics_context_set_compositing_mode(ctx, GCompOpSet);
  graphics_draw_bitmap_in_rect(ctx, checkbox, GRect(cursor.x, cursor.y + (CELL_HEIGHT - checkbox_frame.size.h) / 2, checkbox_frame.size.w, checkbox_frame.size.h));
  cursor.x += checkbox_frame.size.w + IMAGE_MARGIN;

  // draw text
  GFont font = fonts_get_system_font(FONT_KEY_GOTHIC_24_BOLD);
  // TODO: clean up position math
  GRect text_frame = GRect(cursor.x, cursor.y + (CELL_HEIGHT - 24) / 2 - 6, bounds.size.w - cursor.x - 2, 24);
  graphics_draw_text(ctx, task->content + offset, font, text_frame, GTextOverflowModeTrailingEllipsis, GTextAlignmentLeft, NULL);

  // draw line, if task complete
  if (task->complete) {
    GSize content_size = graphics_text_layout_get_content_size(task->content + offset, font, text_frame, GTextOverflowModeTrailingEllipsis, GTextAlignmentLeft);
    graphics_draw_line(ctx, GPoint(cursor.x, CELL_HEIGHT / 2), GPoint(cursor.x + content_size.w, CELL_HEIGHT / 2));
  }
}

static uint16_t menu_layer_get_num_rows(struct MenuLayer *menu_layer, uint16_t section_index, void *callback_context) {
  return (s_tasks) ? s_tasks->size : 0;
}

static void menu_layer_selection_changed(struct MenuLayer *menu_layer, MenuIndex new_index, MenuIndex old_index, void *callback_context) {
  start_scroll();
}

static void menu_layer_select_click(struct MenuLayer *menu_layer, MenuIndex *cell_index, void *callback_context) {
  if (!s_interactive || !s_tasks || cell_index->row > s_tasks->size) return;

  task_t *task = s_tasks->tasks[cell_index->row];
  task->complete = !task->complete;

  menu_layer_reload_data(s_tasks_layer);

  DictionaryIterator *iterator;
  app_message_outbox_begin(&iterator);
  dict_write_uint32(iterator, AppKeyTaskId, task->id);
  app_message_outbox_send();
}

static void menu_layer_select_long_click(struct MenuLayer *menu_layer, MenuIndex *cell_index, void *callback_context) {
#ifdef PBL_MICROPHONE
  if (!s_interactive) return;

  char dictation_buffer[DICTATION_BUFFER_SIZE];
  DictationSession *s_dictation_session = dictation_session_create(sizeof(dictation_buffer), dictation_session_callback, NULL);
  dictation_session_start (s_dictation_session);
#endif
}

static int16_t menu_layer_get_cell_height(struct MenuLayer *menu_layer, MenuIndex *cell_index, void *callback_context) {
  return CELL_HEIGHT;
}

static void dictation_session_callback(DictationSession *session, DictationSessionStatus status, char *transcription, void *context) {
  if (status != DictationSessionStatusSuccess) return;

  DictionaryIterator *iterator;
  app_message_outbox_begin(&iterator);
  dict_write_cstring(iterator, AppKeyTask, transcription);
  app_message_outbox_send();
}

static void start_scroll() {
  if (s_scroll_timer != NULL) app_timer_cancel(s_scroll_timer);

  s_scroll_position = 0;
  s_scroll_timer = app_timer_register(SCROLL_PAUSE, update_scroll, NULL);

  // todo: bounds checking
  uint16_t row = menu_layer_get_selected_index (s_tasks_layer).row;
  s_tasks->tasks[row]->active = true;
}

static void update_scroll(void *data) {
  // todo: bounds checking
  uint16_t row = menu_layer_get_selected_index (s_tasks_layer).row;
  if (!s_tasks->tasks[row]->active) {
    s_scroll_timer = NULL;
    return;
  }

  s_scroll_position++;
  layer_mark_dirty(menu_layer_get_layer(s_tasks_layer));
  s_scroll_timer = app_timer_register(SCROLL_PAUSE, update_scroll, NULL);
}

static void tasks_create(uint8_t num_tasks) {
  tasks_destroy();
  if (num_tasks == 0) return;

  s_tasks = malloc(sizeof(tasks_t));
  s_tasks->capacity = num_tasks;
  s_tasks->size = 0;
  s_tasks->tasks = malloc(s_tasks->capacity * sizeof(task_t *));
  for (int i = 0; i < s_tasks->capacity; ++i) {
    s_tasks->tasks[i] = NULL;
  }
}

static void tasks_append(uint32_t id, char *content) {
  task_t *task = malloc(sizeof(task_t));
  task->id = id;
  task->content = content;
  task->complete = false;

  s_tasks->tasks[s_tasks->size++] = task;
}

static void tasks_destroy() {
  if (s_tasks == NULL) return;

  for (uint8_t i = 0; i < s_tasks->size; ++i) {
    if (s_tasks->tasks[i] == NULL) continue;

    task_t *task = s_tasks->tasks[i];
    free(task->content);
    free(task);
    s_tasks->tasks[i] = NULL;
  }

  free(s_tasks);
  s_tasks = NULL;
}

static void inbox_received_callback(DictionaryIterator *iter, void *context) {
  Tuple *num_tasks_t = dict_find(iter, AppKeyTasksLength);
  if (num_tasks_t)
    tasks_create(num_tasks_t->value->uint8);

  Tuple *item_t = dict_find(iter, AppKeyTask);
  if (item_t) {
    char *item = strdup(item_t->value->cstring);

    Tuple *id_t = dict_find(iter, AppKeyTaskId);
    uint32_t id = id_t->value->uint32;

    tasks_append(id, item);

    Tuple *done_t = dict_find(iter, AppKeyTasksDone);
    if (done_t) {
      s_interactive = true;
      menu_layer_reload_data(s_tasks_layer);
      start_scroll();
    }
  }
}

static void inbox_dropped_callback(AppMessageResult reason, void *context) {
}

static void outbox_failed_callback(DictionaryIterator *iter, AppMessageResult reason, void *context) {
}

static void outbox_sent_callback(DictionaryIterator *iter, void *context) {
}