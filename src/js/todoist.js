var request = require('request');

var Todoist = {
  URLS: {
    getItems: 'https://todoist.com/API/v6/sync?seq_no=0&resource_types=[%22items%22%2C%22projects%22]',
    executeCommands: 'https://todoist.com/API/v6/sync?commands='
  }
};

/* Source: http://stackoverflow.com/questions/105034/create-guid-uuid-in-javascript */
var guid = function () {
  function s4() {
    return Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
  }

  return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() +
  s4() + s4();
}

var sync = function (token, callback) {
  request(Todoist.URLS.getItems + '&token=' + token, 'GET', function (data) {
    callback(JSON.parse(data));
  });
}

var createTask = function (token, content, callback) {
  var command = [{
    type: "item_add",
    temp_id: guid(),
    uuid: guid(),
    args: {
      content: content,
      date_string: "today"
    }
  }]

  request(Todoist.URLS.executeCommands + JSON.stringify(command) + '&token=' + token, 'GET', function (data) {
    callback(JSON.parse(data));
  });
}

var closeTasks = function (token, ids, callback) {
  if (ids.length == 0) return;

  var commands = [];
  for (var i = 0; i < ids.length; ++i) {
    var command = {
      type: "item_close",
      uuid: guid(),
      args: {
        id: ids[i]
      }
    };

    commands.unshift(command);
  }

  request(Todoist.URLS.executeCommands + JSON.stringify(commands) + '&token=' + token, 'GET', function (data) {
    callback(JSON.parse(data));
  });
}

module.exports = {
  sync: sync,
  createTask: createTask,
  closeTasks: closeTasks
};