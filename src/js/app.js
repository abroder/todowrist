var Todoist = require('todoist');

var token = localStorage.token || '';
var projects = JSON.parse(localStorage.projects) || {};

var closeInterval = null;
var toClose = [];

var getTomorrow = function () {
  var tomorrow = new Date();
  tomorrow.setHours(0);
  tomorrow.setMinutes(0);
  tomorrow.setSeconds(0);
  tomorrow.setMilliseconds(0);
  tomorrow.setDate(tomorrow.getDate() + 1);

  return tomorrow;
}

var closeTasks = function () {
  var closing = toClose;
  toClose = [];

  Todoist.closeTasks(token, closing, function (res) {
    sync();
  });
}

var createTask = function (content) {
  Todoist.createTask(token, content, function (res) {
    sync();
  });
}

var sync = function () {
  if (token == undefined || token == null || token.length == 0) {
    sendTasks([{content: 'Configure app', id: 0}]);
    return;
  }

  Todoist.sync(token, function (res) {
    // todo: handle errors
    var actualItems = [];
    var tomorrow = new getTomorrow();
    for (var i = 0; i < res.Items.length; ++i) {
      var item = res.Items[i];

      if (item.due_date && new Date(item.due_date) < tomorrow)
        if (projects[item.project_id]) {
          if (projects[item.project_id].active)
            actualItems.unshift(item);
        } else
            actualItems.unshift(item);

    }

    sendTasks(actualItems);

    for (var i = 0; i < res.Projects.length; ++i) {
      var project = res.Projects[i];
      if (projects[project.id]) {
        projects[project.id].name = project.name;
      } else {
        projects[project.id] = {
          name: project.name,
          active: true
        };
      }
    }
  });
};

var sendTasks = function (tasks) {
  if (tasks.length > 0)
    sendTask(tasks, 0);
};

var sendTask = function (tasks, idx) {
  var dict = {};

  /* Send length of tasks with first task. */
  if (idx == 0)
    dict['AppKeyTasksLength'] = tasks.length;

  /* Let the watch know when the last task has been sent */
  if (idx == tasks.length - 1)
    dict['AppKeyTasksDone'] = true;

  dict['AppKeyTask'] = tasks[idx].content;
  dict['AppKeyTaskId'] = tasks[idx].id;

  Pebble.sendAppMessage(dict, function () {
    idx++;
    if (idx < tasks.length) {
      sendTask(tasks, idx);
    }
  }, function () {

  });
};

Pebble.addEventListener("ready", function () {
  sync();
});

Pebble.addEventListener("appmessage", function (e) {
  if (e.payload["AppKeyTaskId"]) {
    if (closeInterval != null)
      clearTimeout(closeInterval);

    var taskId = e.payload["AppKeyTaskId"];
    if (taskId == 0) {
      sync();
      return;
    }

    var idx = toClose.indexOf(taskId)
    if (idx == -1)
      toClose.unshift(taskId);
    else
      toClose.splice(idx, 1);

    closeInterval = setTimeout(closeTasks, 3000);
  } else if (e.payload["AppKeyTask"]) {
    createTask(e.payload["AppKeyTask"]);
  }
});

Pebble.addEventListener('showConfiguration', function() {
  var config = {
    token: token,
    projects: projects
  };

  var url = '0.0.0.0:8000/?' + encodeURIComponent(JSON.stringify(config));
  Pebble.openURL(url);
});

Pebble.addEventListener('webviewclosed', function(e) {
  var config = JSON.parse(decodeURIComponent(e.response));
  token = localStorage.token = config.token;
  projects = config.projects;
  localStorage.projects = JSON.stringify(config.projects);
  sync();
});